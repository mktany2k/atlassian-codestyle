# Atlassian Code Style

This is a repository for sharing various codestyles for Atlassian code.

## Example Maven configuration
Configure checkstyle so it can be executed with `mvn checkstyle:check` or as
part of a regular build if the plugin is bound.
````
    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-checkstyle-plugin</artifactId>
                    <dependencies>
                        <dependency>
                            <groupId>com.atlassian.codestyle</groupId>
                            <artifactId>platform</artifactId>
                            <version>${platform.checkstyle.rule.version}</version>
                        </dependency>
                    </dependencies>
                    <configuration>
                        <configLocation>atlassian-codestyle-platform/checkstyle-rules.xml</configLocation>
                        <includeTestSourceDirectory>true</includeTestSourceDirectory>
                        <consoleOutput>true</consoleOutput>
                    </configuration>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>

````

Run checkstyle on every build with a profile
````
        <profile>
            <id>codestyle</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-checkstyle-plugin</artifactId>
                        <executions>
                            <execution>
                                <id>verify</id>
                                <phase>validate</phase>
                                <goals>
                                    <goal>check</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
````